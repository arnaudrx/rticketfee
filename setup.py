from distutils.core import setup

setup(
    name='Redmine Ticket Fee',
    version='0.1.0',
    author='A. Roux',
    packages=['rticketfee',],
    scripts=['rticketfee/rticketfee.py',],
    license='LICENSE.txt',
    licenses='DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE',
    description='Useful towel-related stuff.',
    long_description=open('README.rst').read(),
    install_requires=[
        "python-redmine==2.2.1",
        "Click==7.0",
        "requests==2.22.0",
    ],
)