Redmine tickets fee
========================


A simple script for collecting times for Redmine instance. 

.. code-block:: shell

    rticketfee.py --email foo@bar.com  --password '******' --addressurl https://suivi.aixak.fr --project aixak-si --startdate 01-01-2020 --enddate 31-01-2020

Installation
--------------

Simply do:

.. code-block:: shell

    git clone git@bitbucket.org:arnaudrx/rticketfee.git
    cd rticketfee/
    pip install -r requirements.txt && python setup.py install
