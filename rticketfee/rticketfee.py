#!/usr/bin/env python3

import math
from datetime import date
import sys

from redminelib import Redmine
from redminelib.managers import ResourceManager
import click



def tenth_round(nb_hours):
    total_days = nb_hours / 7
    total_quarts_day = total_days / 0.25
    return math.ceil(total_quarts_day) * 0.25

def stdout_output(data, dailyfee, total_hours):
    RED   = "\033[1;31m"  
    BLUE  = "\033[1;34m"
    CYAN  = "\033[1;36m"
    GREEN = "\033[0;32m"
    RESET = "\033[0;0m"
    BOLD    = "\033[;1m"
    REVERSE = "\033[;7m"
    total_data = []
    for issue_complete, days in data:
        sys.stdout.write(RED)
        #print('Issue ID:')
        issue_str = '#' + str(issue_complete.id)
        print(f"[{issue_str}]: ")
        sys.stdout.write(BLUE)
        print(issue_complete.subject)
        sys.stdout.write(CYAN)
        #print(issue_complete.description)
        
        print(f"<{str(days)}>")
        if dailyfee:
            sys.stdout.write(GREEN)
            print('Fee:')
            tarif = str(float(days) * float(dailyfee))
            print(tarif)
            total_data.append(tarif)

        sys.stdout.write(BOLD)
        print('_________________________\n')
        sys.stdout.write(RESET)
    total = str(sum([float(i) for i in total_data]))
    print('TOTAL_HOURS:', total_hours)
    print('TOTAL: %s €' % total)

OUTPUT_MODE = {
    'stdout' : stdout_output
}

def get_object_api(url='', user='', password=''):
    return Redmine(url=url, username=user, password=password)

def get_session_username(redmine_inst):
    user_id = redmine_inst.auth().id
    return user_id


def fetch_time_entry(redmine_instance):
    issue_total_hours = {}
    for time_entry in redmine_instance:
        hours = time_entry.hours
        issue =  time_entry.issue
        propo = str(issue.id)
        #activity = time_entry.activity
        #created_on = time_entry.created_on
        #project=time_entry.project
        #spent_on=time_entry.spent_on
        #updated_on=time_entry. updated_on
        #user = time_entry.user
        if not issue_total_hours.get(propo, False):
            issue_total_hours[propo]= hours
        else:
            issue_total_hours[propo] += hours

    return issue_total_hours
 
@click.command()
@click.option('--email', default='', help='Redmine login.')
@click.option('--password', default='', help='Redmine password.')
@click.option('--addressurl', default='', help='Redmine instance url.')
@click.option('--project', default='', help='Redmine project string ID.')
@click.option('--startdate', default='', help='Start date (jj-MM-AAAA) ex: 12-04-2019')
@click.option('--enddate', default='', help='End date (jj-MM-AAAA) ex: 12-04-2019')
@click.option('--output', default='stdout', help='Output format.')
@click.option('--dailyfee', default=None, help='Daily fee for pricing your ticket.')
def runcli(email, password, addressurl, project, dailyfee, output, startdate, enddate):

    sd = startdate.split('-')
    start = date(int(sd[2]),int(sd[1]), int(sd[0]) )
    ed = enddate.split('-')
    end = date(int(ed[2]),int(ed[1]),int( ed[0]) )
    redmine = Redmine(addressurl, username=email, password=password)
    userid = redmine.auth().id
    redmine_instance = redmine.time_entry.filter(project_id=project ,user_id=userid, from_date=start,  to_date=end )
    issue_total_hours = fetch_time_entry(redmine_instance)
    data = []
    total_hours = 0
    for i, j in issue_total_hours.items():
        total_hours += j
        days = tenth_round(j)
        issue_complete = redmine.issue.get(int(i), include=[])
        data.append(
            (issue_complete, days, )
        )
    OUTPUT_MODE[output](data, dailyfee, total_hours)

if __name__ == '__main__':
    runcli()




